#!/bin/bash

set -u -e

# echo "A3 = $3"

# echo "A4 = $4"

find "$3" -type f | while read -r f; do
    # echo "C: $f"
    # USE TEMPORARY FILE INSTEAD
    # TMPF=$(mktemp -p /tmp)
    FROM=$1
    TMPF=/runs/"${FROM//+([^[:alnum:]_-\.])/_}"-"$(date +%Y-%m-%d-%H:%M+%s.%N)"
    # echo "TMPF = $TMPF"
    # USE TEMPORARY FILE INSTEAD
    # trap 'rm -f $TMPF; exit $?' INT TERM EXIT
    g=$(basename "$f")
    #echo "g = $g"
    echo "File $g:"
    #
    if [ -d "/transforms/$g" ]; then
        (>&2 echo "$g has transforms")
        find "/transforms/$g" "/transforms/all" -type f -exec {} "$f" \;
    elif [ -d "/transforms/default" ]; then
        (>&2 echo "$g has generic transforms")
        find "/transforms/default" "/transforms/all" -type f -exec {} "$f" \;
    else
        true
        (>&2 echo "$g has no transforms")
    fi
    if [ -d "/tests/$g" ]; then
        (>&2 echo "$g has tests")
        find "/tests/$g" "/tests/all" -type f -exec /zeppelingrade/rundocker.sh "$ZEPPELINGRADEID" "$f" {} + > "$TMPF"
    elif [ -d "/tests/default" ]; then
        (>&2 echo "$g has generic tests")
        find "/tests/default" "/tests/all" -type f -exec /zeppelingrade/rundocker.sh "$ZEPPELINGRADEID" "$f" {} + > "$TMPF"
    else
        true
        (>&2 echo "$g has no tests")
    fi
    #if [ -d "/tests/all" ]; then
    #    (>&2 echo "there are tests in /all")
    #    find "/tests/all" -type f -exec /zeppelingrade/rundocker.sh "$ZEPPELINGRADEID" "$f" {} + > "$TMPF"
    #else
    #    true
    #    (>&2 echo "there are no tests in /all")
    #fi
    if [ -d "/checks/$g" ]; then
        (>&2 echo "$g has specific checks")
        # find "/checks/$g" -type f
        # echo "----- RUN ------"

        #if
        find "/checks/$g" "/checks/all" -type f -exec {} "$TMPF" "$4" "$1" "$2" \;
        # then
        #     true
        #     # echo "All scripts run"
        # else
        #     # echo "Some scripts failed to run!"
        #     exit 1
        # fi
    elif [ -d "/checks/default" ]; then
        (>&2 echo "$g has generic checks")
        # find "/checks/default" -type f
        # echo "----- RUN ------"
        #if
        find "/checks/default" "/checks/all" -type f -exec {} "$TMPF" "$4" \;
        # then
        #     true
        #     # echo "All scripts run"
        # else
        #     # echo "Some scripts failed to run!"
        #     exit 1
        # fi
    else
        true
        (>&2 echo "$g has no checks, and there are no generic ones")
    fi
    #if [ -d "/checks/all" ]; then
    #    (>&2 echo "there are checks in /all")
        # find "/checks/$g" -type f
        # echo "----- RUN ------"

        #if
    #    find "/checks/all" -type f -exec {} "$TMPF" "$4" \;
        # then
        #     true
        #     # echo "All scripts run"
        # else
        #     # echo "Some scripts failed to run!"
        #     exit 1
        # fi
    #else
    #    true
    #    (>&2 echo "there are no checks in /all")
    #fi
    # USE TEMPORARY FILE INSTEAD
    #trap - INT TERM EXIT
    #rm -f "$TMPF"
done
