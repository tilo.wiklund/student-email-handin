# Install (Docker Hub)

Find my latest build [here](https://cloud.docker.com/repository/docker/tilowiklund/autoemail).

# Install (Build)

Root folder contains main Dockerfile
```
docker build -t autoemail .
```

# Email setup (Uppsala University and similar)

When running specify the following environment variables:

EMAILSERVER: imap server to poll (mail.uu.se)

EMAILUSER: username for the imap server (abcwi123@user.uu.se)

EMAILPASS: password for the imap server

DRAFTFOLDER: folder in which to put the draft responses

INFOLDER: folder from which to fetch assignment hand-in

for example through a docker env-file (see [example_env_file](example_env_file)):
```
docker run --env-file env_file ... autoemail
```

# Email setup (Alternative)

If the template offlineimap config is incompatiable with your server you can
manually write an offlineimaprc (see [offlineimap_template.sh](offlineimap_template.sh) for the
outline) and put it in `/offlineimaprc` inside the container.

# Scripts to run

By default it does nothing but download each email message. You have to specify
what to do with each email message by putting *bash* scripts in `/scripts`. For
each new (meaning unread/unseen) email message in `INFOLDER` each script in
`/scripts` is run once: as follows
```
/scripts/script.sh $FROM $SUBJECT $INATTACHDIR $OUTATTACHDIR
```
where `FROM` is the sender, `SUBJECT` is the message subject, `INATTACHDIR` is a
temporary folder containing all the files attached to the email, and
`OUTATTACHDIR` is a temporary folder into which the script can put any files to
be included in the response email.

If any script fails (exit code not equal to 0) it is interpreted as an error in
*processing*, not a mistake in the solution. The email is left untouched and
will be re-processed later.

All scripts succeeding (exit codes equal to 0) is interpreted as the email being
successfully *processed*. A response email will be put in `DRAFTFOLDER`
containing the script STDOUT/STDERR in its body and all files put in
`OUTATTACHDIR` as attachments.

Scripts are most easily attached using a volume
```
docker run --volume=/path/to/scripts:/scripts ... autoemail
```

# Example scripts

A simple example script that always succeeds, greets the student, and attaches a
file `/examplefile` could look like:
```sh
#!/bin/env bash

echo "Dear $1, you recently send me an email concerning $2 and gave me the files:"
ls $3
echo "The attached file contains all you need to know."
echo "Best,"
echo "Prof. XYZ"

cp /examplefile $4/pleaselookatme

exit 0
```

If multiple homeworks are active at the same time you can determine which one
to grade by for example inspecting the subject line and/or which files were attached:
```sh
#!/bin/env bash

if echo "$2" | grep -ie 'assignment 1'; then
  echo "Thanks for handing in assignment 1!"
  if [ !  -f $3/assign1.py ]; then
     echo "But you appear to have forgotten to attach assign1.py :("
     exit 0
  else
     echo "We ran your assignment"
     result = "$(python $3/assign1.py)"
     if echo "${result}" | grep -ie 'OK'; then
       echo "And it appears to run fine. Please check the attached example solution."
       cp /files/solution_assign1.py $4/solution_assign1.py
       exit 0
     else
       echo "But it did not say OK! Please make sure the your solution is ok and try again."
       echo "The output was:"
       echo "${result}"
       exit 0
     fi
  fi
elif echo "$2" | grep -ie 'assignment 2'; then
  ...
else
  echo "Thanks for handing in a solution, but it is unclear which homework you solved!"
  echo "Please indicate by including 'Assignment X' in the subject line."
fi
```
